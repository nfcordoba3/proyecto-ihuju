/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ihuju.nightlife.general;

import java.util.Calendar;
import java.util.Date;

/**
 *
 * @author danny
 */
public class EJBCommons {

    private static EJBCommons instance = new EJBCommons();

    private EJBCommons() {

    }

    public static EJBCommons getInstance() {
        return instance;
    }

    public Date getDateMinusDay(Date date) {
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        cal.add(Calendar.DATE, -1);
        return cal.getTime();
    }

    public Date addYearToDate(Date date) {
        Calendar c = Calendar.getInstance();
        c.setTime(date);
        c.add(Calendar.YEAR, 1);
        return c.getTime();
    }

}
