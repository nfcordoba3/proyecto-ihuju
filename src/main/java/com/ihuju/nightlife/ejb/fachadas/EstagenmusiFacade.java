/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ihuju.nightlife.ejb.fachadas;

import com.ihuju.nightlife.jpa.entidades.Estagenmusi;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 *
 * @author Win8
 */
@Stateless
public class EstagenmusiFacade extends AbstractFacade<Estagenmusi> {

    @PersistenceContext(unitName = "com.ihuju_Nightlife_war_1.0.0PU")
    private EntityManager em;

    @Override
    protected EntityManager getEntityManager() {
        return em;
    }

    public EstagenmusiFacade() {
        super(Estagenmusi.class);
    }
    
}
