/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ihuju.nightlife.jpa.enums;

/**
 *
 * @author danny
 */
public enum Rol {

    ADMIN("ADMIN", 1),
    ANALISTA("ANALISTA", 2),
    CONSULTA("CONSULTA", 3);
    
    private final String tipo;
    private final int idRol;

    Rol(String tipo, int idRol) {
        this.tipo = tipo;
        this.idRol = idRol;
    }

    public String getTipo() {
        return tipo;
    }

    public int getIdRol() {
        return idRol;
    }

    public static Rol getRol(int id) {

        switch (id) {
            case 1:
                return Rol.ADMIN;
            case 2:
                return Rol.ANALISTA;
            default:
                return Rol.CONSULTA;
        }
    }
}
