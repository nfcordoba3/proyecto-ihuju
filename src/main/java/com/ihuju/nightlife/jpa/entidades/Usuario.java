/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.ihuju.nightlife.jpa.entidades;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import java.util.List;
import javax.persistence.Basic;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;
import javax.persistence.UniqueConstraint;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;

/**
 *
 * @author Win8
 */
@Entity
@Table(name = "USUARIO", catalog = "", schema = "NIGTHLIFE", uniqueConstraints = {
    @UniqueConstraint(columnNames = {"NOMBREUSUARIO"})})
@XmlRootElement
@NamedQueries({
    @NamedQuery(name = "Usuario.findAll", query = "SELECT u FROM Usuario u")
    , @NamedQuery(name = "Usuario.findByPkUsuario", query = "SELECT u FROM Usuario u WHERE u.pkUsuario = :pkUsuario")
    , @NamedQuery(name = "Usuario.findByNombreusuario", query = "SELECT u FROM Usuario u WHERE u.nombreusuario = :nombreusuario")
    , @NamedQuery(name = "Usuario.findByContrasenia", query = "SELECT u FROM Usuario u WHERE u.contrasenia = :contrasenia")})
public class Usuario implements Serializable {

    private static final long serialVersionUID = 1L;
    // @Max(value=?)  @Min(value=?)//if you know range of your decimal fields consider using these annotations to enforce field validation
    @Id
    @Basic(optional = false)
    @NotNull
    @Column(name = "PK_USUARIO", nullable = false, precision = 0, scale = -127)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "SQESTADOUSUARIO")
    @SequenceGenerator(name = "SQESTADOUSUARIO", sequenceName = "SQESTADOUSUARIO",  initialValue = 3, allocationSize = 100)
    private BigDecimal pkUsuario;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "NOMBREUSUARIO", nullable = false, length = 200)
    private String nombreusuario;
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 200)
    @Column(name = "CONTRASENIA", nullable = false, length = 200)
    private String contrasenia;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkUsuario")
    private List<Estadousuario> estadousuarioList;
    @Column(name ="ULTIMOACCESO")
    @Temporal(TemporalType.TIMESTAMP)
    private Date ultimoAcceso;   
    @Size(max=45)
    @Column (name="CODACTIVACION")
    private String codActivacion;
    
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkUsuario")
    private List<Cliente> clienteList;
    @JoinColumn(name = "FK_PERSONA", referencedColumnName = "PK_PERSONA")
    @ManyToOne
    private Persona fkPersona;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkUsuario")
    private List<Duenio> duenioList;
    @OneToMany(cascade = CascadeType.ALL, mappedBy = "fkUsuario")
    private List<Accperusu> accperusuList;
    @OneToMany(mappedBy = "fkUsuario")
    private List<Artista> artistaList;

    public Usuario() {
    }

    public Usuario(BigDecimal pkUsuario) {
        this.pkUsuario = pkUsuario;
    }

    public Usuario(BigDecimal pkUsuario, String nombreusuario, String contrasenia) {
        this.pkUsuario = pkUsuario;
        this.nombreusuario = nombreusuario;
        this.contrasenia = contrasenia;
    }

    public BigDecimal getPkUsuario() {
        return pkUsuario;
    }

    public void setPkUsuario(BigDecimal pkUsuario) {
        this.pkUsuario = pkUsuario;
    }

    public String getNombreusuario() {
        return nombreusuario;
    }

    public void setNombreusuario(String nombreusuario) {
        this.nombreusuario = nombreusuario;
    }

    public Date getUltimoAcceso() {
        return ultimoAcceso;
    }

    public void setUltimoAcceso(Date ultimoAcceso) {
        this.ultimoAcceso = ultimoAcceso;
    }

    public String getCodActivacion() {
        return codActivacion;
    }

    public void setCodActivacion(String codActivacion) {
        this.codActivacion = codActivacion;
    }

    
    public String getContrasenia() {
        return contrasenia;
    }

    public void setContrasenia(String contrasenia) {
        this.contrasenia = contrasenia;
    }

    @XmlTransient
    public List<Estadousuario> getEstadousuarioList() {
        return estadousuarioList;
    }

    public void setEstadousuarioList(List<Estadousuario> estadousuarioList) {
        this.estadousuarioList = estadousuarioList;
    }

    @XmlTransient
    public List<Cliente> getClienteList() {
        return clienteList;
    }

    public void setClienteList(List<Cliente> clienteList) {
        this.clienteList = clienteList;
    }

    public Persona getFkPersona() {
        return fkPersona;
    }

    public void setFkPersona(Persona fkPersona) {
        this.fkPersona = fkPersona;
    }

    @XmlTransient
    public List<Duenio> getDuenioList() {
        return duenioList;
    }

    public void setDuenioList(List<Duenio> duenioList) {
        this.duenioList = duenioList;
    }

    @XmlTransient
    public List<Accperusu> getAccperusuList() {
        return accperusuList;
    }

    public void setAccperusuList(List<Accperusu> accperusuList) {
        this.accperusuList = accperusuList;
    }

    @XmlTransient
    public List<Artista> getArtistaList() {
        return artistaList;
    }

    public void setArtistaList(List<Artista> artistaList) {
        this.artistaList = artistaList;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (pkUsuario != null ? pkUsuario.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        // TODO: Warning - this method won't work in the case the id fields are not set
        if (!(object instanceof Usuario)) {
            return false;
        }
        Usuario other = (Usuario) object;
        if ((this.pkUsuario == null && other.pkUsuario != null) || (this.pkUsuario != null && !this.pkUsuario.equals(other.pkUsuario))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "com.ihuju.nightlife.jpa.entidades.Usuario[ pkUsuario=" + pkUsuario + " ]";
    }
    
}
