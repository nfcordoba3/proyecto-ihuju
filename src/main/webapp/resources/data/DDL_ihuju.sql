/*==============================================================*/
/* DBMS name:      ORACLE Version 11g                           */
/* Created on:     19/02/2019 4:46:25 p. m.                     */
/*==============================================================*/


alter table ACCIONPERMITIDA
   drop constraint FK_ACCIONPE_REFERENCE_ACCIONPE;

alter table ACCPERPLAN
   drop constraint FK_ACCPERPL_REFERENCE_PLANTILL;

alter table ACCPERPLAN
   drop constraint FK_ACCPERPL_REFERENCE_ACCIONPE;

alter table ACCPERUSU
   drop constraint FK_ACCPERUS_REFERENCE_USUARIO;

alter table ACCPERUSU
   drop constraint FK_ACCPERUS_REFERENCE_ACCIONPE;

alter table ARTISTA
   drop constraint FK_ARTISTA_REFERENCE_USUARIO;

alter table ARTISTA
   drop constraint FK_ARTISTA_REFERENCE_TIPOARTI;

alter table ASISTE
   drop constraint FK_ASISTE_REFERENCE_CLIENTE;

alter table ASISTE
   drop constraint FK_ASISTE_REFERENCE_EVENTO;

alter table CLIENTE
   drop constraint FK_CLIENTE_REFERENCE_USUARIO;

alter table DEPARTAMENTO
   drop constraint FK_DEPARTAM_REFERENCE_PAIS;

alter table DUENIO
   drop constraint FK_DUENIO_REFERENCE_USUARIO;

alter table ESTABLECIMIENTO
   drop constraint FK_ESTABLEC_REFERENCE_DUENIO;

alter table ESTABLECIMIENTO
   drop constraint FK_ESTABLEC_REFERENCE_MUNICIPI;

alter table ESTADISPO
   drop constraint FK_ESTADISP_REFERENCE_ESTABLEC;

alter table ESTADISPO
   drop constraint FK_ESTADISP_REFERENCE_DISPONIB;

alter table ESTADOUSUARIO
   drop constraint FK_ESTADOUS_REFERENCE_USUARIO;

alter table ESTADOUSUARIO
   drop constraint FK_ESTADOUS_REFERENCE_ESTADO;

alter table ESTAGENMUSI
   drop constraint FK_ESTAGENM_REFERENCE_GENEROMU;

alter table ESTAGENMUSI
   drop constraint FK_ESTAGENM_REFERENCE_ESTABLEC;

alter table ESTATIPGEN
   drop constraint FK_ESTATIPG_REFERENCE_TIPOGENE;

alter table ESTATIPGEN
   drop constraint FK_ESTATIPG_REFERENCE_ESTABLEC;

alter table EVENTO
   drop constraint FK_EVENTO_REFERENCE_ESTABLEC;

alter table EVENTO
   drop constraint FK_EVENTO_REFERENCE_TIPOEVEN;

alter table MUNICIPIO
   drop constraint FK_MUNICIPI_REFERENCE_DEPARTAM;

alter table PARTICIPA
   drop constraint FK_PARTICIP_REFERENCE_CRONOGRA;

alter table PARTICIPA
   drop constraint FK_PARTICIP_REFERENCE_ARTISTA;

alter table PARTICIPA
   drop constraint FK_PARTICIP_REFERENCE_EVENTO;

alter table PERSONA
   drop constraint FK_PERSONA_REFERENCE_TIPOIDEN;

alter table PERSONA
   drop constraint FK_PERSONA_REFERENCE_MUNICIPI;

alter table PERSONA
   drop constraint FK_PERSONA_REFERENCE_ESTADOCI;

alter table PERSONA
   drop constraint FK_PERSONA_REFERENCE_GENERO;

alter table RESERVA
   drop constraint FK_RESERVA_REFERENCE_EVENTO;

alter table RESERVA
   drop constraint FK_RESERVA_REFERENCE_CLIENTE;

alter table TELEFONO
   drop constraint FK_TELEFONO_REFERENCE_TIPOTELE;

alter table TELEFONO
   drop constraint FK_TELEFONO_REFERENCE_PERSONA;

alter table USUARIO
   drop constraint FK_USUARIO_REFERENCE_PERSONA;

drop index UNIQUE_CODIGOACCPER;

drop table ACCIONPERMITIDA cascade constraints;

drop table ACCPERPLAN cascade constraints;

drop table ACCPERUSU cascade constraints;

drop table ARTISTA cascade constraints;

drop table ASISTE cascade constraints;

drop table CLIENTE cascade constraints;

drop table CRONOGRAMA cascade constraints;

drop index UNIQUE_CODIGODEPARTAMENTO;

drop table DEPARTAMENTO cascade constraints;

drop table DISPONIBILIDAD cascade constraints;

drop table DUENIO cascade constraints;

drop index UNIQUE_NITESTABLECIMIENTO;

drop table ESTABLECIMIENTO cascade constraints;

drop table ESTADISPO cascade constraints;

drop index UNIQUE_CODIGOESTADO;

drop table ESTADO cascade constraints;

drop index UNIQUE_CODESTACIVIL;

drop table ESTADOCIVIL cascade constraints;

drop table ESTADOUSUARIO cascade constraints;

drop table ESTAGENMUSI cascade constraints;

drop table ESTATIPGEN cascade constraints;

drop table EVENTO cascade constraints;

drop index UNIQUE_GENERO;

drop table GENERO cascade constraints;

drop table GENEROMUSICA cascade constraints;

drop index UNIQUE_MUNICIPIOCODNOM;

drop table MUNICIPIO cascade constraints;

drop index UNIQUE_CODIGOPAIS;

drop table PAIS cascade constraints;

drop table PARTICIPA cascade constraints;

drop table PERSONA cascade constraints;

drop table PLANTILLA cascade constraints;

drop table RESERVA cascade constraints;

drop table TELEFONO cascade constraints;

drop table TIPOARTISTA cascade constraints;

drop table TIPOEVENTO cascade constraints;

drop table TIPOGENERO cascade constraints;

drop index UNIQUE_CODIGOTIPIDENTIFI;

drop table TIPOIDENTIFICACION cascade constraints;

drop index UNIQUE_CODIGOTIPTELEFONO;

drop table TIPOTELEFONO cascade constraints;

drop index UNIQUE_NOMUSU;

drop table USUARIO cascade constraints;

drop sequence SQESTADOCIVIL;

drop sequence SQ_ACCIONPERUSUARIO;

drop sequence SQ_ACCIPERMITIDA;

drop sequence SQ_ACCIPERPLAN;

drop sequence SQ_ARTISTA;

drop sequence SQ_ASISTE;

drop sequence SQ_CLIENTE;

drop sequence SQ_CRONOGRAMA;

drop sequence SQ_DEPARTAMENTO;

drop sequence SQ_DISPONIBILIDAD;

drop sequence SQ_DUENIO;

drop sequence SQ_ESTABLECIMIENTO;

drop sequence SQ_ESTADISPO;

drop sequence SQ_ESTADO;

drop sequence SQ_ESTADOCIVIL;

drop sequence SQ_ESTADOUSUARIO;

drop sequence SQ_ESTAGENMUSI;

drop sequence SQ_ESTATIPGEN;

drop sequence SQ_EVENTO;

drop sequence SQ_GENERO;

drop sequence SQ_GENEROMUSICA;

drop sequence SQ_MUNICIPIO;

drop sequence SQ_PAIS;

drop sequence SQ_PARTICIPA;

drop sequence SQ_PERSONA;

drop sequence SQ_PLANTILLA;

drop sequence SQ_RESERVA;

drop sequence SQ_TELEFONO;

drop sequence SQ_TIPOARTISTA;

drop sequence SQ_TIPOEVENTO;

drop sequence SQ_TIPOGENERO;

drop sequence SQ_TIPOIDENTIFICACION;

drop sequence SQ_TIPOTELEFONO;

drop sequence SQ_USUARIO;

create sequence SQESTADOCIVIL
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_ACCIONPERUSUARIO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_ACCIPERMITIDA
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_ACCIPERPLAN
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_ARTISTA
increment by 1
start with 1
nocycle
 nocache;

create sequence SQ_ASISTE
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_CLIENTE
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_CRONOGRAMA
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_DEPARTAMENTO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_DISPONIBILIDAD
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_DUENIO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_ESTABLECIMIENTO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_ESTADISPO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_ESTADO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_ESTADOCIVIL
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_ESTADOUSUARIO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_ESTAGENMUSI
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_ESTATIPGEN
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_EVENTO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_GENERO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_GENEROMUSICA
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_MUNICIPIO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_PAIS
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_PARTICIPA
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_PERSONA
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_PLANTILLA
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_RESERVA
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_TELEFONO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_TIPOARTISTA
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_TIPOEVENTO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_TIPOGENERO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_TIPOIDENTIFICACION
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_TIPOTELEFONO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

create sequence SQ_USUARIO
increment by 1
start with 1
 minvalue 1
nocycle
 nocache;

/*==============================================================*/
/* Table: ACCIONPERMITIDA                                       */
/*==============================================================*/
create table ACCIONPERMITIDA 
(
   PK_ACCIPERMITIDA     NUMBER               not null,
   FK_ACCIPERMITIDA     NUMBER,
   CODIGO               VARCHAR2(20)         not null,
   DESCRIPCION          VARCHAR2(500)        not null,
   RUTA                 VARCHAR2(500)        not null,
   constraint PK_ACCIONPERMITIDA primary key (PK_ACCIPERMITIDA)
);

/*==============================================================*/
/* Index: UNIQUE_CODIGOACCPER                                   */
/*==============================================================*/
create unique index UNIQUE_CODIGOACCPER on ACCIONPERMITIDA (
   CODIGO ASC
);

/*==============================================================*/
/* Table: ACCPERPLAN                                            */
/*==============================================================*/
create table ACCPERPLAN 
(
   PK_ACCIPERPLA        NUMBER(6)            not null,
   FK_PLANTILLA         NUMBER               not null,
   PK_ACCIPERMITIDA     NUMBER,
   constraint PK_ACCPERPLAN primary key (PK_ACCIPERPLA)
);

/*==============================================================*/
/* Table: ACCPERUSU                                             */
/*==============================================================*/
create table ACCPERUSU 
(
   PK_ACCIPERUSU        NUMBER               not null,
   FK_USUARIO           NUMBER               not null,
   FK_ACCIPERMITIDA     NUMBER               not null,
   constraint PK_ACCPERUSU primary key (PK_ACCIPERUSU)
);

/*==============================================================*/
/* Table: ARTISTA                                               */
/*==============================================================*/
create table ARTISTA 
(
   PK_ARTISTA           NUMBER               not null,
   FK_TIPARTIS          NUMBER,
   FK_USUARIO           NUMBER,
   APODO                VARCHAR2(100)        not null,
   GENEROMUSICAL        VARCHAR2(100)        not null,
   EXPERIENCIA          VARCHAR2(100)        not null,
   constraint PK_ARTISTA primary key (PK_ARTISTA)
);

/*==============================================================*/
/* Table: ASISTE                                                */
/*==============================================================*/
create table ASISTE 
(
   PK_ASISTE            NUMBER               not null,
   FK_CLIENTE           NUMBER               not null,
   FK_EVENTO            NUMBER               not null,
   constraint PK_ASISTE primary key (PK_ASISTE)
);

/*==============================================================*/
/* Table: CLIENTE                                               */
/*==============================================================*/
create table CLIENTE 
(
   PK_CLIENTE           NUMBER               not null,
   FK_USUARIO           NUMBER               not null,
   constraint PK_CLIENTE primary key (PK_CLIENTE)
);

/*==============================================================*/
/* Table: CRONOGRAMA                                            */
/*==============================================================*/
create table CRONOGRAMA 
(
   PK_CRONOGRA          NUMBER               not null,
   HORA                 DATE                 not null,
   DESCRIPCION          VARCHAR2(100)        not null,
   constraint PK_CRONOGRAMA primary key (PK_CRONOGRA)
);

/*==============================================================*/
/* Table: DEPARTAMENTO                                          */
/*==============================================================*/
create table DEPARTAMENTO 
(
   PK_DEPARTAMEN        NUMBER               not null,
   FK_PAIS              NUMBER               not null,
   CODIGO               VARCHAR2(20)         not null,
   NOMBRE               VARCHAR2(100)        not null,
   constraint PK_DEPARTAMENTO primary key (PK_DEPARTAMEN)
);

/*==============================================================*/
/* Index: UNIQUE_CODIGODEPARTAMENTO                             */
/*==============================================================*/
create unique index UNIQUE_CODIGODEPARTAMENTO on DEPARTAMENTO (
   CODIGO ASC
);

/*==============================================================*/
/* Table: DISPONIBILIDAD                                        */
/*==============================================================*/
create table DISPONIBILIDAD 
(
   PK_DISPONIBILIDAD    NUMBER               not null,
   HORARIO              DATE                 not null,
   constraint PK_DISPONIBILIDAD primary key (PK_DISPONIBILIDAD)
);

/*==============================================================*/
/* Table: DUENIO                                                */
/*==============================================================*/
create table DUENIO 
(
   PK_DUENIO            NUMBER               not null,
   FK_USUARIO           NUMBER               not null,
   constraint PK_DUENIO primary key (PK_DUENIO)
);

/*==============================================================*/
/* Table: ESTABLECIMIENTO                                       */
/*==============================================================*/
create table ESTABLECIMIENTO 
(
   PK_ESTABLEC          NUMBER               not null,
   FK_DUENIO            NUMBER               not null,
   FK_MUNICILUGAR       NUMBER               not null,
   DIRECCION            VARCHAR2(500)        not null,
   NOMBRE               VARCHAR2(200)        not null,
   NIT                  VARCHAR2(200)        not null,
   CAPACIDAD            NUMBER               not null,
   PRECIO               NUMBER               not null,
   constraint PK_ESTABLECIMIENTO primary key (PK_ESTABLEC)
);

/*==============================================================*/
/* Index: UNIQUE_NITESTABLECIMIENTO                             */
/*==============================================================*/
create index UNIQUE_NITESTABLECIMIENTO on ESTABLECIMIENTO (
   NIT ASC
);

/*==============================================================*/
/* Table: ESTADISPO                                             */
/*==============================================================*/
create table ESTADISPO 
(
   PK_ESTADISPO         NUMBER               not null,
   FK_ESTABLEC          NUMBER               not null,
   FK_DISPONIBILIDAD    NUMBER               not null,
   constraint PK_ESTADISPO primary key (PK_ESTADISPO)
);

/*==============================================================*/
/* Table: ESTADO                                                */
/*==============================================================*/
create table ESTADO 
(
   PK_ESTADO            NUMBER               not null,
   CODIGO               VARCHAR2(20)         not null,
   DESCRIPCION          VARCHAR2(100)        not null,
   constraint PK_ESTADO primary key (PK_ESTADO)
);

/*==============================================================*/
/* Index: UNIQUE_CODIGOESTADO                                   */
/*==============================================================*/
create unique index UNIQUE_CODIGOESTADO on ESTADO (
   CODIGO ASC
);

/*==============================================================*/
/* Table: ESTADOCIVIL                                           */
/*==============================================================*/
create table ESTADOCIVIL 
(
   PK_ESTADOCIVIL       NUMBER               not null,
   CODIGO               VARCHAR2(20)         not null,
   DECRIPCION           VARCHAR2(100),
   constraint PK_ESTADOCIVIL primary key (PK_ESTADOCIVIL)
);

/*==============================================================*/
/* Index: UNIQUE_CODESTACIVIL                                   */
/*==============================================================*/
create unique index UNIQUE_CODESTACIVIL on ESTADOCIVIL (
   CODIGO ASC
);

/*==============================================================*/
/* Table: ESTADOUSUARIO                                         */
/*==============================================================*/
create table ESTADOUSUARIO 
(
   PK_ESTADOUSUARIO     NUMBER               not null,
   FK_USUARIO           NUMBER               not null,
   FK_ESTADO            NUMBER               not null,
   FECHAINICIO          DATE                 not null,
   FECHAFIN             DATE,
   constraint PK_ESTADOUSUARIO primary key (PK_ESTADOUSUARIO)
);

/*==============================================================*/
/* Table: ESTAGENMUSI                                           */
/*==============================================================*/
create table ESTAGENMUSI 
(
   PK_ESTAGENMUSI       NUMBER               not null,
   FK_GENEROMUSICA      NUMBER               not null,
   FK_ESTABLEC          NUMBER               not null,
   constraint PK_ESTAGENMUSI primary key (PK_ESTAGENMUSI)
);

/*==============================================================*/
/* Table: ESTATIPGEN                                            */
/*==============================================================*/
create table ESTATIPGEN 
(
   PK_ESTATIPGEN        NUMBER               not null,
   FK_TIPOGENERO        NUMBER               not null,
   FK_ESTABLEC          NUMBER               not null,
   constraint PK_ESTATIPGEN primary key (PK_ESTATIPGEN)
);

/*==============================================================*/
/* Table: EVENTO                                                */
/*==============================================================*/
create table EVENTO 
(
   PK_EVENTO            NUMBER               not null,
   FK_ESTABLEC          NUMBER               not null,
   FK_TIPOEVENTO        NUMBER               not null,
   FECHA                DATE                 not null,
   DESCRIPCION          CLOB                 not null,
   CAPACIDAD            NUMBER               not null,
   PRECIO               NUMBER               not null,
   constraint PK_EVENTO primary key (PK_EVENTO)
);

/*==============================================================*/
/* Table: GENERO                                                */
/*==============================================================*/
create table GENERO 
(
   PK_GENERO            NUMBER(6)            not null,
   CODIGO               VARCHAR2(20)         not null,
   DESCRIPCION          VARCHAR2(100)        not null,
   constraint PK_GENERO primary key (PK_GENERO)
);

/*==============================================================*/
/* Index: UNIQUE_GENERO                                         */
/*==============================================================*/
create unique index UNIQUE_GENERO on GENERO (
   CODIGO ASC
);

/*==============================================================*/
/* Table: GENEROMUSICA                                          */
/*==============================================================*/
create table GENEROMUSICA 
(
   PK_GENEROMUSICA      NUMBER               not null,
   NOMBRE               VARCHAR2(100)        not null,
   DESCRIPCION          VARCHAR2(100)        not null,
   constraint PK_GENEROMUSICA primary key (PK_GENEROMUSICA)
);

/*==============================================================*/
/* Table: MUNICIPIO                                             */
/*==============================================================*/
create table MUNICIPIO 
(
   PK_MUNICIPIO         NUMBER               not null,
   FK_DEPARTAMEN        NUMBER               not null,
   CODIGO               VARCHAR2(20)         not null,
   NOMBRE               VARCHAR2(100)        not null,
   constraint PK_MUNICIPIO primary key (PK_MUNICIPIO)
);

/*==============================================================*/
/* Index: UNIQUE_MUNICIPIOCODNOM                                */
/*==============================================================*/
create unique index UNIQUE_MUNICIPIOCODNOM on MUNICIPIO (
   CODIGO ASC,
   NOMBRE ASC
);

/*==============================================================*/
/* Table: PAIS                                                  */
/*==============================================================*/
create table PAIS 
(
   PK_PAIS              NUMBER               not null,
   CODIGO               VARCHAR2(20)         not null,
   NOMBRE               VARCHAR2(100)        not null,
   constraint PK_PAIS primary key (PK_PAIS)
);

/*==============================================================*/
/* Index: UNIQUE_CODIGOPAIS                                     */
/*==============================================================*/
create unique index UNIQUE_CODIGOPAIS on PAIS (
   CODIGO ASC
);

/*==============================================================*/
/* Table: PARTICIPA                                             */
/*==============================================================*/
create table PARTICIPA 
(
   PK_PARTICIPA         NUMBER               not null,
   FK_ARTISTA           NUMBER               not null,
   FK_EVENTO            NUMBER               not null,
   FK_CRONOGRA          NUMBER               not null,
   constraint PK_PARTICIPA primary key (PK_PARTICIPA)
);

/*==============================================================*/
/* Table: PERSONA                                               */
/*==============================================================*/
create table PERSONA 
(
   PK_PERSONA           NUMBER               not null,
   FK_TIPOIDENTIFI      NUMBER               not null,
   FK_MUNICIPIO         NUMBER               not null,
   PK_ESTADOCIVIL       NUMBER,
   PK_GENERO            NUMBER(6)            not null,
   IDENTIFICACION       VARCHAR2(200)        not null,
   NOMBRE               VARCHAR2(500)        not null,
   APELLIDO             VARCHAR2(500)        not null,
   EMAIL                VARCHAR2(100),
   FECHANACIMIENTO      DATE                 not null,
   constraint PK_PERSONA primary key (PK_PERSONA)
);

/*==============================================================*/
/* Table: PLANTILLA                                             */
/*==============================================================*/
create table PLANTILLA 
(
   PK_PLANTILLA         NUMBER               not null,
   CODIGO               VARCHAR2(20)         not null,
   DESCRIPCION          VARCHAR2(100)        not null,
   constraint PK_PLANTILLA primary key (PK_PLANTILLA)
);

/*==============================================================*/
/* Table: RESERVA                                               */
/*==============================================================*/
create table RESERVA 
(
   PK_RESERVA           NUMBER               not null,
   FK_CLIENTE           NUMBER               not null,
   FK_EVENTO            NUMBER               not null,
   HORA                 DATE                 not null,
   CANPERSONAS          NUMBER               not null,
   constraint PK_RESERVA primary key (PK_RESERVA)
);

/*==============================================================*/
/* Table: TELEFONO                                              */
/*==============================================================*/
create table TELEFONO 
(
   PK_TELEFONO          NUMBER               not null,
   FK_TIPTELEFONO       NUMBER               not null,
   FK_PERSONA           NUMBER,
   NUMERO               NUMBER(30)           not null,
   constraint PK_TELEFONO primary key (PK_TELEFONO)
);

/*==============================================================*/
/* Table: TIPOARTISTA                                           */
/*==============================================================*/
create table TIPOARTISTA 
(
   PK_TIPARTIS          NUMBER               not null,
   NOMBRE               VARCHAR2(100)        not null,
   DESCRIPCION          VARCHAR2(100)        not null,
   constraint PK_TIPOARTISTA primary key (PK_TIPARTIS)
);

/*==============================================================*/
/* Table: TIPOEVENTO                                            */
/*==============================================================*/
create table TIPOEVENTO 
(
   PK_TIPOEVENTO        NUMBER               not null,
   NOMBRE               VARCHAR2(100)        not null,
   DESCRIPCION          CLOB                 not null,
   constraint PK_TIPOEVENTO primary key (PK_TIPOEVENTO)
);

/*==============================================================*/
/* Table: TIPOGENERO                                            */
/*==============================================================*/
create table TIPOGENERO 
(
   PK_TIPOGENERO        NUMBER               not null,
   NOMBRE               VARCHAR2(100)        not null,
   DESCRIPCION          VARCHAR2(100)        not null,
   constraint PK_TIPOGENERO primary key (PK_TIPOGENERO)
);

/*==============================================================*/
/* Table: TIPOIDENTIFICACION                                    */
/*==============================================================*/
create table TIPOIDENTIFICACION 
(
   PK_TIPOIDENTIFI      NUMBER               not null,
   CODIGO               VARCHAR2(20)         not null,
   DESCRIPCION          VARCHAR2(100)        not null,
   constraint PK_TIPOIDENTIFICACION primary key (PK_TIPOIDENTIFI)
);

/*==============================================================*/
/* Index: UNIQUE_CODIGOTIPIDENTIFI                              */
/*==============================================================*/
create unique index UNIQUE_CODIGOTIPIDENTIFI on TIPOIDENTIFICACION (
   CODIGO ASC
);

/*==============================================================*/
/* Table: TIPOTELEFONO                                          */
/*==============================================================*/
create table TIPOTELEFONO 
(
   PK_TIPTELEFONO       NUMBER               not null,
   CODIGO               VARCHAR2(20)         not null,
   DESCRIPCION          VARCHAR2(100)        not null,
   constraint PK_TIPOTELEFONO primary key (PK_TIPTELEFONO)
);

/*==============================================================*/
/* Index: UNIQUE_CODIGOTIPTELEFONO                              */
/*==============================================================*/
create unique index UNIQUE_CODIGOTIPTELEFONO on TIPOTELEFONO (
   CODIGO ASC
);

/*==============================================================*/
/* Table: USUARIO                                               */
/*==============================================================*/
create table USUARIO 
(
   PK_USUARIO           NUMBER               not null,
   FK_PERSONA           NUMBER,
   NOMBREUSUARIO        VARCHAR2(200)        not null,
   CONTRASENIA          VARCHAR2(200)        not null,
   constraint PK_USUARIO primary key (PK_USUARIO)
);

/*==============================================================*/
/* Index: UNIQUE_NOMUSU                                         */
/*==============================================================*/
create unique index UNIQUE_NOMUSU on USUARIO (
   NOMBREUSUARIO ASC
);

alter table ACCIONPERMITIDA
   add constraint FK_ACCIONPE_REFERENCE_ACCIONPE foreign key (FK_ACCIPERMITIDA)
      references ACCIONPERMITIDA (PK_ACCIPERMITIDA);

alter table ACCPERPLAN
   add constraint FK_ACCPERPL_REFERENCE_PLANTILL foreign key (FK_PLANTILLA)
      references PLANTILLA (PK_PLANTILLA);

alter table ACCPERPLAN
   add constraint FK_ACCPERPL_REFERENCE_ACCIONPE foreign key (PK_ACCIPERMITIDA)
      references ACCIONPERMITIDA (PK_ACCIPERMITIDA);

alter table ACCPERUSU
   add constraint FK_ACCPERUS_REFERENCE_USUARIO foreign key (FK_USUARIO)
      references USUARIO (PK_USUARIO);

alter table ACCPERUSU
   add constraint FK_ACCPERUS_REFERENCE_ACCIONPE foreign key (FK_ACCIPERMITIDA)
      references ACCIONPERMITIDA (PK_ACCIPERMITIDA);

alter table ARTISTA
   add constraint FK_ARTISTA_REFERENCE_USUARIO foreign key (FK_USUARIO)
      references USUARIO (PK_USUARIO);

alter table ARTISTA
   add constraint FK_ARTISTA_REFERENCE_TIPOARTI foreign key (FK_TIPARTIS)
      references TIPOARTISTA (PK_TIPARTIS);

alter table ASISTE
   add constraint FK_ASISTE_REFERENCE_CLIENTE foreign key (FK_CLIENTE)
      references CLIENTE (PK_CLIENTE);

alter table ASISTE
   add constraint FK_ASISTE_REFERENCE_EVENTO foreign key (FK_EVENTO)
      references EVENTO (PK_EVENTO);

alter table CLIENTE
   add constraint FK_CLIENTE_REFERENCE_USUARIO foreign key (FK_USUARIO)
      references USUARIO (PK_USUARIO);

alter table DEPARTAMENTO
   add constraint FK_DEPARTAM_REFERENCE_PAIS foreign key (FK_PAIS)
      references PAIS (PK_PAIS);

alter table DUENIO
   add constraint FK_DUENIO_REFERENCE_USUARIO foreign key (FK_USUARIO)
      references USUARIO (PK_USUARIO);

alter table ESTABLECIMIENTO
   add constraint FK_ESTABLEC_REFERENCE_DUENIO foreign key (FK_DUENIO)
      references DUENIO (PK_DUENIO);

alter table ESTABLECIMIENTO
   add constraint FK_ESTABLEC_REFERENCE_MUNICIPI foreign key (FK_MUNICILUGAR)
      references MUNICIPIO (PK_MUNICIPIO);

alter table ESTADISPO
   add constraint FK_ESTADISP_REFERENCE_ESTABLEC foreign key (FK_ESTABLEC)
      references ESTABLECIMIENTO (PK_ESTABLEC);

alter table ESTADISPO
   add constraint FK_ESTADISP_REFERENCE_DISPONIB foreign key (FK_DISPONIBILIDAD)
      references DISPONIBILIDAD (PK_DISPONIBILIDAD);

alter table ESTADOUSUARIO
   add constraint FK_ESTADOUS_REFERENCE_USUARIO foreign key (FK_USUARIO)
      references USUARIO (PK_USUARIO);

alter table ESTADOUSUARIO
   add constraint FK_ESTADOUS_REFERENCE_ESTADO foreign key (FK_ESTADO)
      references ESTADO (PK_ESTADO);

alter table ESTAGENMUSI
   add constraint FK_ESTAGENM_REFERENCE_GENEROMU foreign key (FK_GENEROMUSICA)
      references GENEROMUSICA (PK_GENEROMUSICA);

alter table ESTAGENMUSI
   add constraint FK_ESTAGENM_REFERENCE_ESTABLEC foreign key (FK_ESTABLEC)
      references ESTABLECIMIENTO (PK_ESTABLEC);

alter table ESTATIPGEN
   add constraint FK_ESTATIPG_REFERENCE_TIPOGENE foreign key (FK_TIPOGENERO)
      references TIPOGENERO (PK_TIPOGENERO);

alter table ESTATIPGEN
   add constraint FK_ESTATIPG_REFERENCE_ESTABLEC foreign key (FK_ESTABLEC)
      references ESTABLECIMIENTO (PK_ESTABLEC);

alter table EVENTO
   add constraint FK_EVENTO_REFERENCE_ESTABLEC foreign key (FK_ESTABLEC)
      references ESTABLECIMIENTO (PK_ESTABLEC);

alter table EVENTO
   add constraint FK_EVENTO_REFERENCE_TIPOEVEN foreign key (FK_TIPOEVENTO)
      references TIPOEVENTO (PK_TIPOEVENTO);

alter table MUNICIPIO
   add constraint FK_MUNICIPI_REFERENCE_DEPARTAM foreign key (FK_DEPARTAMEN)
      references DEPARTAMENTO (PK_DEPARTAMEN);

alter table PARTICIPA
   add constraint FK_PARTICIP_REFERENCE_CRONOGRA foreign key (FK_CRONOGRA)
      references CRONOGRAMA (PK_CRONOGRA);

alter table PARTICIPA
   add constraint FK_PARTICIP_REFERENCE_ARTISTA foreign key (FK_ARTISTA)
      references ARTISTA (PK_ARTISTA);

alter table PARTICIPA
   add constraint FK_PARTICIP_REFERENCE_EVENTO foreign key (FK_EVENTO)
      references EVENTO (PK_EVENTO);

alter table PERSONA
   add constraint FK_PERSONA_REFERENCE_TIPOIDEN foreign key (FK_TIPOIDENTIFI)
      references TIPOIDENTIFICACION (PK_TIPOIDENTIFI);

alter table PERSONA
   add constraint FK_PERSONA_REFERENCE_MUNICIPI foreign key (FK_MUNICIPIO)
      references MUNICIPIO (PK_MUNICIPIO);

alter table PERSONA
   add constraint FK_PERSONA_REFERENCE_ESTADOCI foreign key (PK_ESTADOCIVIL)
      references ESTADOCIVIL (PK_ESTADOCIVIL);

alter table PERSONA
   add constraint FK_PERSONA_REFERENCE_GENERO foreign key (PK_GENERO)
      references GENERO (PK_GENERO);

alter table RESERVA
   add constraint FK_RESERVA_REFERENCE_EVENTO foreign key (FK_EVENTO)
      references EVENTO (PK_EVENTO);

alter table RESERVA
   add constraint FK_RESERVA_REFERENCE_CLIENTE foreign key (FK_CLIENTE)
      references CLIENTE (PK_CLIENTE);

alter table TELEFONO
   add constraint FK_TELEFONO_REFERENCE_TIPOTELE foreign key (FK_TIPTELEFONO)
      references TIPOTELEFONO (PK_TIPTELEFONO);

alter table TELEFONO
   add constraint FK_TELEFONO_REFERENCE_PERSONA foreign key (FK_PERSONA)
      references PERSONA (PK_PERSONA);

alter table USUARIO
   add constraint FK_USUARIO_REFERENCE_PERSONA foreign key (FK_PERSONA)
      references PERSONA (PK_PERSONA);

